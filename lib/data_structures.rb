# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  arr.max-arr.min
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  str.downcase.count("aeiou")
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  #vowels = ['a', 'e', 'i', 'o', 'u']
  str.downcase.delete("aeiou")

end

# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.chars.sort.reverse!
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  unique = str.downcase.chars.uniq.join
  str.downcase != unique

end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  first = arr[0..2].join
  second = arr[3..5].join
  third = arr[6..9].join

  '(' + first + ') ' + second + '-' + third
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  arr = str.delete(',')
  new_array = []
  arr.chars.each do |char|
    new_array << char.to_i
  end
  new_array.max - new_array.min
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  # your code goes here
  #take(num) takes the first num-1 elements of an array
  #drop(num) takes only the rest of the array from num and after
  if offset > arr.length
    offset = offset % arr.length
  end
  beginning = arr[0...offset]
  ending = arr[offset..-1]
  ending + beginning
end
